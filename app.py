import flask

app = flask.Flask(__name__)

@app.route('/')
def root():
    mensagem = 'Programação de Redes com Python!'
    return flask.render_template('index.html', mensagem = mensagem)

@app.route('/upper',methods=['post'])
def toUpper():
    mensagem = flask.request.get_json()
    mensagem['message'] = mensagem['message'].upper()
    return mensagem

if __name__ == '__main__':
    app.run('0.0.0.0', 80)